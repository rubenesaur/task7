const express = require('express');
const app = express();
const port = 5000;
const db = require('./queries');

app.use(express.json());
app.use(express.urlencoded({extended: true}));



app.get('/', (request, response) =>{
    response.json({ info: "Node.js, Express, and Postgres API"});
});

app.get('/api/v1/planets',db.getPlanets);
app.post('/api/v1/planets/add',db.createPlanet);
app.delete('/api/v1/planets/delete',db.deletePlanet);
app.put('/api/v1/planets/update',db.updatePlanet);

app.listen(port, ()=> {
    console.log(`App running on port ${port}`);
});
