const Pool = require('pg').Pool;
const pool = new Pool({
    user: "planetuser",
    host: "localhost",
    database: "planets",
    password: "password",
    port: "5432"
});

const getPlanets = (request,response) =>{
    pool.query('SELECT * FROM planetstab',(error,results)=>{
        if(error){
            throw error;
        }
        response.status(200).json(results.rows);
    });
}

const createPlanet = (request,response) =>{
    const {name, size, datediscovered} = request.body;
    pool.query('INSERT INTO planetstab (name, size, datediscovered) VALUES ($1, $2, $3) RETURNING *', [name, size, datediscovered], (error, results)=>{
        if(error){
            throw error;
        }
        //response.status(201).send(`User added with ID: ${results.rows.id}`);
        response.status(200).json(results.rows);
    });
}

const deletePlanet = (request,response)=>{
    const id = request.query.id;
    pool.query('DELETE FROM planetstab WHERE id=$1',[id],(error,results)=>{
        if(error){
            throw error;
        }
        response.status(200).send(`User deleted with ID: ${id}`);
    });
}

const updatePlanet = (request,response)=>{
    const id = request.query.id;
    const { name, size, datediscovered } = request.body;

    pool.query('UPDATE planetstab SET name = $1, size = $2, datediscovered = $3 WHERE id = $4',
    [name,size,datediscovered,id],(error,results)=>{
        if(error){
            throw error;
        }
        response.status(200).send(`User modified with id: ${id}`);
    });
}

module.exports = {
    getPlanets,
    createPlanet,
    deletePlanet,
    updatePlanet,
}